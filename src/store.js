let prevDateStr = "2024-07-15";
let curDateStr = "2024-08-15";
const snapshotList = [
    {
        dateStr: "2023-10-15",
        values: [
            {
                counterKey: "coldWater-kitchen",
                resourceKey: "coldWater",
                value: 38.1,
            },
            {
                counterKey: "coldWater-bathroom",
                resourceKey: "coldWater",
                value: 269.6,
            },
            {
                counterKey: "hotWater-kitchen",
                resourceKey: "hotWater",
                value: 36.2,
            },
            {
                counterKey: "hotWater-bathroom",
                resourceKey: "hotWater",
                value: 41.3,
            },
            {
                counterKey: "electricity-day",
                resourceKey: "electricity",
                value: 69.6,
            },
            {
                counterKey: "electricity-night",
                resourceKey: "electricity",
                value: 12.4,
            },
        ],
    },
    {
        dateStr: "2023-11-15",
        values: [
            {
                counterKey: "coldWater-kitchen",
                resourceKey: "coldWater",
                value: 38.5,
            },
            {
                counterKey: "coldWater-bathroom",
                resourceKey: "coldWater",
                value: 272.4,
            },
            {
                counterKey: "hotWater-kitchen",
                resourceKey: "hotWater",
                value: 37.2,
            },
            {
                counterKey: "hotWater-bathroom",
                resourceKey: "hotWater",
                value: 41.3,
            },
            {
                counterKey: "electricity-day",
                resourceKey: "electricity",
                value: 149.9,
            },
            {
                counterKey: "electricity-night",
                resourceKey: "electricity",
                value: 22.2,
            },
        ],
    },

    {
        dateStr: "2023-12-15",
        values: [
            {
                counterKey: "coldWater-kitchen",
                resourceKey: "coldWater",
                value: 38.8,
            },
            {
                counterKey: "coldWater-bathroom",
                resourceKey: "coldWater",
                value: 275.5,
            },
            {
                counterKey: "hotWater-kitchen",
                resourceKey: "hotWater",
                value: 38.0,
            },
            {
                counterKey: "hotWater-bathroom",
                resourceKey: "hotWater",
                value: 41.3,
            },
            {
                counterKey: "electricity-day",
                resourceKey: "electricity",
                value: 231.6,
            },
            {
                counterKey: "electricity-night",
                resourceKey: "electricity",
                value: 33.0,
            },
        ],
    },

    {
        dateStr: "2024-01-19",
        values: [
            {
                counterKey: "coldWater-kitchen",
                resourceKey: "coldWater",
                value: 39.0,
            },
            {
                counterKey: "coldWater-bathroom",
                resourceKey: "coldWater",
                value: 278.7,
            },
            {
                counterKey: "hotWater-kitchen",
                resourceKey: "hotWater",
                value: 38.9,
            },
            {
                counterKey: "hotWater-bathroom",
                resourceKey: "hotWater",
                value: 41.3,
            },
            {
                counterKey: "electricity-day",
                resourceKey: "electricity",
                value: 314.3,
            },
            {
                counterKey: "electricity-night",
                resourceKey: "electricity",
                value: 48.1,
            },
        ],
    },
    {
        dateStr: "2024-02-16",
        values: [
            {
                counterKey: "coldWater-kitchen",
                resourceKey: "coldWater",
                value: 39.1,
            },
            {
                counterKey: "coldWater-bathroom",
                resourceKey: "coldWater",
                value: 279.8,
            },
            {
                counterKey: "hotWater-kitchen",
                resourceKey: "hotWater",
                value: 39.1,
            },
            {
                counterKey: "hotWater-bathroom",
                resourceKey: "hotWater",
                value: 41.3,
            },
            {
                counterKey: "electricity-day",
                resourceKey: "electricity",
                value: 347.3,
            },
            {
                counterKey: "electricity-night",
                resourceKey: "electricity",
                value: 56.4,
            },
        ],
        bills: [
            {
                title: "АО ГВ",
                value: 15.35,
                group: "",
            },
            {
                title: "АО ТЕ",
                value: 31.07,
                group: "",
            },
            {
                title: "АО ХВВ",
                value: 39.19,
                group: "",
            },
            {
                title: "Вивезення побутових відходів",
                value: 43.11,
                group: "",
            },
            {
                title: "Управління багатоквартирним будинком",
                value: 303.63,
                group: "",
            },
            {
                title: "Централізоване опалення",
                value: 1089.59,
                group: "",
            },
            {
                title: "Відшкодування витрат з електроенергії",
                value: 39.15,
                group: "",
            },
        ],
    },
    {
        dateStr: "2024-03-15",
        values: [
            {
                counterKey: "coldWater-kitchen",
                resourceKey: "coldWater",
                value: 39.1,
            },
            {
                counterKey: "coldWater-bathroom",
                resourceKey: "coldWater",
                value: 280.0,
            },
            {
                counterKey: "hotWater-kitchen",
                resourceKey: "hotWater",
                value: 39.1,
            },
            {
                counterKey: "hotWater-bathroom",
                resourceKey: "hotWater",
                value: 41.3,
            },
            {
                counterKey: "electricity-day",
                resourceKey: "electricity",
                value: 362.9,
            },
            {
                counterKey: "electricity-night",
                resourceKey: "electricity",
                value: 62.7,
            },
        ],
        bills: [
            {
                title: "АО ГВ",
                value: 15.35,
                group: "",
            },
            {
                title: "АО ТЕ",
                value: 31.07,
                group: "",
            },
            {
                title: "АО ХВВ",
                value: 39.19,
                group: "",
            },
            {
                title: "Вивезення побутових відходів",
                value: 43.45,
                group: "",
            },
            {
                title: "Управління багатоквартирним будинком",
                value: 303.63,
                group: "",
            },
            {
                title: "Централізоване опалення",
                value: 951.12,
                group: "",
            },
            {
                title: "Відшкодування витрат з електроенергії",
                value: 39.15,
                group: "",
            },
        ],
    },
    {
        dateStr: "2024-04-15",
        values: [
            {
                counterKey: "coldWater-kitchen",
                resourceKey: "coldWater",
                value: 39.1,
            },
            {
                counterKey: "coldWater-bathroom",
                resourceKey: "coldWater",
                value: 280.1,
            },
            {
                counterKey: "hotWater-kitchen",
                resourceKey: "hotWater",
                value: 39.1,
            },
            {
                counterKey: "hotWater-bathroom",
                resourceKey: "hotWater",
                value: 41.3,
            },
            {
                counterKey: "electricity-day",
                resourceKey: "electricity",
                value: 376.0,
            },
            {
                counterKey: "electricity-night",
                resourceKey: "electricity",
                value: 68.8,
            },
        ],
        bills: [
            {
                title: "АО ГВ",
                value: 15.35,
                group: "",
            },
            {
                title: "АО ТЕ",
                value: 31.07,
                group: "",
            },
            {
                title: "АО ХВВ",
                value: 39.19,
                group: "",
            },
            {
                title: "Централізоване опалення",
                value: 881.64,
                group: "",
            },
            {
                title: "Вивезення побутових відходів",
                value: 39.28,
                group: "",
            },
            {
                title: "Управління багатоквартирним будинком",
                value: 303.63,
                group: "",
            },
            {
                title: "Відшкодування витрат з електроенергії",
                value: 39.15,
                group: "",
            },
        ],
    },
    {
        dateStr: "2024-05-17",
        values: [
            {
                counterKey: "coldWater-kitchen",
                resourceKey: "coldWater",
                value: 39.1,
            },
            {
                counterKey: "coldWater-bathroom",
                resourceKey: "coldWater",
                value: 280.1,
            },
            {
                counterKey: "hotWater-kitchen",
                resourceKey: "hotWater",
                value: 39.1,
            },
            {
                counterKey: "hotWater-bathroom",
                resourceKey: "hotWater",
                value: 41.3,
            },
            {
                counterKey: "electricity-day",
                resourceKey: "electricity",
                value: 388.7,
            },
            {
                counterKey: "electricity-night",
                resourceKey: "electricity",
                value: 74.9,
            },
        ],
        bills: [
            {
                title: "АО ГВ",
                value: 15.35,
                group: "",
            },
            {
                title: "АО ТЕ",
                value: 31.07,
                group: "",
            },
            {
                title: "АО ХВВ",
                value: 39.19,
                group: "",
            },
            {
                title: "Централізоване опалення",
                value: 200.35,
                group: "",
            },
            {
                title: "Вивезення побутових відходів",
                value: 43.32,
                group: "",
            },
            {
                title: "Управління багатоквартирним будинком",
                value: 303.63,
                group: "",
            },
            {
                title: "Відшкодування витрат з електроенергії",
                value: 39.15,
                group: "",
            },
        ],
    },
    {
        dateStr: "2024-06-15",
        values: [
            {
                counterKey: "coldWater-kitchen",
                resourceKey: "coldWater",
                value: 39.1,
            },
            {
                counterKey: "coldWater-bathroom",
                resourceKey: "coldWater",
                value: 280.1,
            },
            {
                counterKey: "hotWater-kitchen",
                resourceKey: "hotWater",
                value: 39.1,
            },
            {
                counterKey: "hotWater-bathroom",
                resourceKey: "hotWater",
                value: 41.3,
            },
            {
                counterKey: "electricity-day",
                resourceKey: "electricity",
                value: 402.2,
            },
            {
                counterKey: "electricity-night",
                resourceKey: "electricity",
                value: 81.7,
            },
        ],
        bills: [
            {
                key: "ao-hotv",
                title: "АО ГВ",
                value: 15.35, // копія з минулого місяця
                group: "",
                note: "копія з минулого місяця",
            },
            {
                key: "ao-te",
                title: "АО ТЕ",
                value: 31.07, // копія з минулого місяця
                group: "",
                note: "копія з минулого місяця",
            },
            {
                key: "ao-hvv",
                title: "АО ХВВ",
                value: 47.52,
            },
            {
                key: "opal",
                title: "Централізоване опалення",
                value: 195.22,
                group: "",
            },
            {
                key: "vpv",
                title: "Вивезення побутових відходів",
                value: 43.32, // копія з минулого місяця
                group: "",
                note: "копія з минулого місяця",
            },
            {
                key: "ubb",
                title: "Управління багатоквартирним будинком",
                value: 303.63, // копія з минулого місяця
                note: "копія з минулого місяця",
            },
            {
                key: "vve",
                title: "Відшкодування витрат з електроенергії",
                value: 39.15, // копія з минулого місяця
                note: "копія з минулого місяця",
            },
        ],
        billsCorrected: [
            {
                key: "ao-hotv",
                title: "АО ГВ",
                value: 22.08,
            },
            {
                key: "ao-te",
                title: "АО ТЕ",
                value: 37.25,
            },
            {
                key: "vpv",
                title: "Вивезення побутових відходів",
                value: 41.25,
            },
            {
                key: "vvrno",
                title: "Відшкодування витрат на ремонт насосного обладнання",
                value: 107.01,
            },
            {
                key: "ubb",
                title: "Управління багатоквартирним будинком",
                value: 303.63,
            },
            {
                key: "vve",
                title: "Відшкодування витрат з електроенергії",
                value: 39.15,
            },
        ],
    },
    {
        dateStr: "2024-07-15",
        values: [
            {
                counterKey: "coldWater-kitchen",
                resourceKey: "coldWater",
                value: 39.1,
            },
            {
                counterKey: "coldWater-bathroom",
                resourceKey: "coldWater",
                value: 280.1,
            },
            {
                counterKey: "hotWater-kitchen",
                resourceKey: "hotWater",
                value: 39.1,
            },
            {
                counterKey: "hotWater-bathroom",
                resourceKey: "hotWater",
                value: 41.3,
            },
            {
                counterKey: "electricity-day",
                resourceKey: "electricity",
                value: 416.4,
            },
            {
                counterKey: "electricity-night",
                resourceKey: "electricity",
                value: 89.2,
            },
        ],
        bills: [
            {
                key: "ao-hotv",
                title: "АО ГВ",
                value: 22.08,
            },
            {
                key: "ao-te",
                title: "АО ТЕ",
                value: 37.25,
            },
            {
                key: "ao-hvv",
                title: "АО ХВВ",
                value: 47.52,
            },
            {
                key: "opal",
                title: "Централізоване опалення",
                value: 13.9,
            },
            {
                key: "vpv",
                title: "Вивезення побутових відходів",
                value: 43.07,
            },
            {
                key: "ubb",
                title: "Управління багатоквартирним будинком",
                value: 303.63,
            },
            {
                key: "vve",
                title: "Відшкодування витрат з електроенергії",
                value: 94.83,
            },
        ],
        additional: [
            {
                title: "Недоплата за минулий раз",
                value: 117.85,
            },
        ],
    },
    {
        dateStr: "2024-08-15",
        values: [
            {
                counterKey: "coldWater-kitchen",
                resourceKey: "coldWater",
                value: 39.1,
            },
            {
                counterKey: "coldWater-bathroom",
                resourceKey: "coldWater",
                value: 280.1,
            },
            {
                counterKey: "hotWater-kitchen",
                resourceKey: "hotWater",
                value: 39.1,
            },
            {
                counterKey: "hotWater-bathroom",
                resourceKey: "hotWater",
                value: 41.3,
            },
            {
                counterKey: "electricity-day",
                resourceKey: "electricity",
                value: 423.9,
            },
            {
                counterKey: "electricity-night",
                resourceKey: "electricity",
                value: 92.3,
            },
        ],
        bills: [
            {
                key: "ao-hotv",
                title: "АО ГВ",
                value: 22.08,
            },
            {
                key: "ao-te",
                title: "АО ТЕ",
                value: 37.25,
            },
            {
                key: "ao-hvv",
                title: "АО ХВВ",
                value: 47.52,
            },
            {
                key: "opal",
                title: "Централізоване опалення",
                value: 8.27,
            },
            {
                key: "vpv",
                title: "Вивезення побутових відходів",
                value: 40.78,
            },
            {
                key: "ubb",
                title: "Управління багатоквартирним будинком",
                value: 303.63,
            },
            {
                key: "vve",
                title: "Відшкодування витрат з електроенергії",
                value: 94.83,
            },
        ],
        additional: [],
    },
];

export function setDates(prevDateString, curDateString) {
    prevDateStr = prevDateString
    curDateStr = curDateString
}

export function getCurrentBills() {
    return currentSnapshot.bills;
}

export function getCurrentCorrectedBills() {
    return currentSnapshot.billsCorrected || [];
}

export function hasAdditionalPayments() {
    return getCurrentAdditionalPayments().length > 0
}

export function getCurrentAdditionalPayments() {
    return currentSnapshot.additional || [];
}

const initPrevSnapshot = () => {
    // console.log(['date', prevDateStr]);
    let obj = snapshotList.find((item) => item.dateStr === prevDateStr);

    obj.map = Object.fromEntries(obj.values.map((val) => [val.counterKey, val]));

    obj.resourceMap = obj.values.reduce((map, item) => {
        if (!map.hasOwnProperty(item.resourceKey)) {
            map[item.resourceKey] = [];
        }
        map[item.resourceKey].push(item);

        return map;
    }, {});

    return obj;
};
export const prevSnapshot = new Proxy({initialized: false}, {
    get(target, p, receiver) {
        if (target.initialized === false) {
            Object.assign(target, initPrevSnapshot())
            target.initialized = true
        }

        return Reflect.get(target, p)
    }
});

const initCurrentSnapshot = () => {
    let obj = snapshotList.find((item) => item.dateStr === curDateStr);

    obj.map = Object.fromEntries(obj.values.map((val) => [val.counterKey, val]));

    obj.resourceMap = obj.values.reduce((map, item) => {
        if (!map.hasOwnProperty(item.resourceKey)) {
            map[item.resourceKey] = [];
        }
        map[item.resourceKey].push(item);

        return map;
    }, {});

    return obj;
};
export const currentSnapshot = new Proxy({initialized: false}, {
    get(target, p, receiver) {
        if (target.initialized === false) {
            Object.assign(target, initCurrentSnapshot())
            target.initialized = true
        }

        return Reflect.get(target, p)
    }
});

/*export function diffResourceNumber(resourceKey) {
    const currentSum = currentSnapshot.resourceMap[resourceKey].reduce(
        (sum, item) => {
            return sum + item.value;
        },
        0
    );

    const prevSum = prevSnapshot.resourceMap[resourceKey].reduce((sum, item) => {
        return sum + item.value;
    }, 0);

    return currentSum - prevSum;
}*/

export function prevValueFixed(key) {
    return prevSnapshot.map[key].value.toFixed(1);
}

export function curValueFixed(key) {
    return currentSnapshot.map[key].value.toFixed(1);
}

function round(number, digits = 1) {
    let temp = number * Math.pow(10, digits);
    temp = Math.round(temp);

    return temp / Math.pow(10, digits);
}

export function diff(key) {
    return diffNumber(key).toFixed(1);
}

export function diffNumber(key) {
    return round(currentSnapshot.map[key].value - prevSnapshot.map[key].value);
}

export function diffSum(keys) {
    return diffSumNumber(keys).toFixed(1);
}

export function diffSumNumber(keys) {
    return keys.reduce((sum, key) => {
        return sum + diffNumber(key);
    }, 0);
}

export function getAdditionalTotal() {
    return getCurrentAdditionalPayments().reduce((sum, {value}) => {
        return sum + value;
    }, 0);
}

export function getCounterKeys() {
    return [
        'coldWater',
        'hotWater',
        'electricityNight',
        'electricityDay',
    ];
}

export function getTariffMap() {
    return {
        coldWater: {
            rate: 30.384,
            units: 'м3',
            rateUnits: 'грн.',
        },
        hotWater: {
            rate: 112.11, // 97.89 + 14.22
            units: 'м3',
            rateUnits: 'грн.',
        },
        /*electricityNight: {
          rate: 1.32,
          units: 'кВт*год',
          rateUnits: 'грн.',
        },
        electricityDay: {
          rate: 2.64,
          units: 'кВт*год',
          rateUnits: 'грн.',
        },*/
        electricityNight: {
            rate: 2.16,
            units: 'кВт*год',
            rateUnits: 'грн.',
            since: '2024-07-16',
        },
        electricityDay: {
            rate: 4.32,
            units: 'кВт*год',
            rateUnits: 'грн.',
            since: '2024-07-16',
        },
    };
}

export function getDiffs() {
    return [
        {
            key: 'Холодна вода (кухня)',
            diff: diffNumber('coldWater-kitchen'),
            rateKey: 'coldWater',
        },
        {
            key: 'Холодна вода (ванна)',
            diff: diffNumber('coldWater-bathroom'),
            rateKey: 'coldWater',
        },
        {
            key: 'Гаряча вода (кухня)',
            diff: diffNumber('hotWater-kitchen'),
            rateKey: 'hotWater',
        },
        {
            key: 'Гаряча вода (ванна)',
            diff: diffNumber('hotWater-bathroom'),
            rateKey: 'hotWater',
        },
        {
            key: 'Електроенергія (ніч)',
            diff: diffNumber('electricity-night'),
            rateKey: 'electricityNight',
        },
        {
            key: 'Електроенергія (день)',
            diff: diffNumber('electricity-day'),
            rateKey: 'electricityDay',
        },
    ]
}

export function getMainTotal() {
    // console.log(this.totalList);

    let total = 0;

    const tariffMap = getTariffMap();
    const diffs = getDiffs();

    const counterSum = getCounterKeys().reduce((sum, key) => {
        const rate = tariffMap[key].rate;
        const values = diffs.filter((item) => item.rateKey === key);
        const diffTotal = values.reduce((total, item) => total + item.diff, 0);
        const result = diffTotal * rate;

        return sum + result;
    }, 0);

    total += counterSum;

    total += getCurrentBills().reduce((sum, bill) => {
        return sum + bill.value;
    }, 0);

    return total;
}

export function getGrandTotal() {
    return getMainTotal() + getAdditionalTotal()
}
